import React, { useState } from 'react'

const Counter = (props: any) =>  {
  const [count, setCount] = useState<number>(0);
  return (
    <div>
      <p>Counter: {count}</p>
      <button onClick={() => setCount(count + 1)}>Add</button>
    </div>
    
  )
}

export default Counter